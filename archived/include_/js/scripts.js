// Confirm that user is not visiting thru CDN or incorrect aliased URL
function checkUrl() {
	if ( location.href != "http://welcome-back.info/" )
	location.href =  "http://welcome-back.info/" ;
}
// Dynamically keep the Search Bar / Quote of the day towards the center (vertically) of the screen
function adjustFeature(){
	var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
	var adjusted = ( h - 100 ) / 2 ;
	document.getElementById("feature").style.paddingTop = adjusted + "px" ;
}
// Depending on day, load a quote from array (whose index matches that of current day)
function greetUser() {
	var day = new Date();
	var weekday=new Array(7);
	weekday[0]="Sunday should primarily be a day of rest.";
	weekday[1]="Since it's Monday, it's time to get moving!";
	weekday[2]="Tuesday's here. Don't lose focus.";
	weekday[3]="What is today? It's Prince Spaghetti Day.";
	weekday[4]="Thursday is a great day to be productive.";
	weekday[5]="Friday night is a good time to get ahead.";
	weekday[6]="Saturday can be used to get ahead.";
	document.getElementById("quote").innerHTML=weekday[day.getDay()];
}
// For now, browser page title is the date
function changeTitle() {
	var time  = new Date();
	var month = time.getMonth() + 1;
	var day   = time.getDate();
	var year  = time.getFullYear();
	document.title = " " + month + "/" + day + "/" + year + " - Startpage" ;
}
// Choose random background on each page load from array of backgrounds
function initBackground(){
	var choice = new Array(4);
		choice[0] = "frost_twig";
		choice[1] = "trail";
		choice[2] = "bluff";
		choice[3] = "gondola";
		choice[4] = "flower";
	// Math.floor(Math.random() * (max - min + 1)) + min; <- Random number template
	var num = Math.floor(Math.random() * (4 - 0 + 1)) + 0 ;
	var loc = "url(\'http://welcome-back.info/include_/images/" + choice[num] + ".jpg\')";
	document.getElementById("background").style.backgroundImage=loc;
	backgroundLocation(num);
	return ;
}
// For each background, load corresponding map of location and link to Google Maps via Modal overlay
function backgroundLocation(num) {
    var url, modal;
	var key = "AIzaSyDV4r0y5EeG1ddXV5VoKLxJYJeQ_qcxdHk"
	var choice = new Array(4);
		choice[0] = "44.157628,-71.698887";
		choice[1] = "44.042274,-71.642527";
		choice[2] = "44.181156,-71.697579";
		choice[3] = "44.042864,-71.627898";
		choice[4] = "43.568225,-71.990468";
    url = "https://maps.googleapis.com/maps/api/staticmap?center=" + choice[num] + "&markers=" + choice[num] + "&zoom=7&size=150x150&sensor=false&key=" + key;
	modal = "https://www.google.com/maps/embed/v1/view?key=" + key + "&center=" + choice[num] + "&zoom=15&maptype=satellite";
    document.getElementById('map').innerHTML = '<img style="margin-bottom: -4px;" src="' + url + '" />' ;
	document.getElementById('imgloc').href = modal ;
}
// Initialize the 8Tracks music player
function initMusic() {
	setTimeout(function(){
		var url;
		url = "https://8tracks.com/mixes/5446808/player_v3_universal";
		document.getElementById("musicPlayer").src = url;
	}, 500);
}
// Destroy the music player
function destroyMusic() {
	document.getElementById("musicPlayer").src = "about:blank";
	document.getElementById("musicPlayer").style.display = "none";
}
// Load alternative music playlists
function switchMusic(id) {
    var mix = new Array(5);
    mix[0] = "677414";
    mix[1] = "3394063";
    mix[2] = "3394290";
    mix[3] = "3394355";
    mix[4] = "3394421";
	mix[5] = "5446808";
    document.getElementById("musicPlayer").src = "https://8tracks.com/mixes/" + mix[id] + "/player_v3_universal/autoplay" ;
}
function myMap(location) {
    var ll, url;
    ll = [location.coords.latitude, location.coords.longitude].join(',');
    if ( ll == null ) return ;
    url = "https://maps.googleapis.com/maps/api/staticmap?center=" + ll + "&markers=" + ll + "&zoom=10&size=100x100&sensor=false&key=AIzaSyB2elLJSZuVWk6gFQ1xYEm6IKVyGSlMR_g";
    //document.getElementById("map").src = url;
    document.getElementById('map').innerHTML = '<img style="margin-bottom: -4px;" src="' + url + '" />' ;
}
function setGeoPosition() {
    navigator.geolocation.getCurrentPosition(myMap);
}
